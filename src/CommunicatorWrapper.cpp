#include "parcer/AllClassWrappers.h"

#include "parcer/Eigen.h"
#include "parcer/Communicator.h"

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#if PARCER_HAS_MPI4PY==1
#include <mpi4py/mpi4py.h>
#endif

using namespace parcer;
namespace py = pybind11;

#if PARCER_HAS_MPI4PY
pybind11::handle CallGetComm(Communicator *comm) {
    const int rc = import_mpi4py();
    return pybind11::handle(PyMPIComm_New(comm->GetComm()));;
}
#endif

/*.def("Run", (std::shared_ptr<SampleCollection>  (SamplingAlgorithm::*)(Eigen::VectorXd const&)) &SamplingAlgorithm::Run,
             py::call_guard<py::scoped_ostream_redirect,py::scoped_estream_redirect>())*/

void PythonBindings::CommunicatorWrapper(pybind11::module &m) {
  py::class_<Communicator, std::shared_ptr<Communicator> > commWrap(m, "Communicator");

  commWrap.def(py::init<>());
  commWrap.def(py::init<int const, int const>());
#if PARCER_HAS_MPI4PY
  commWrap.def(py::init( [](pybind11::handle const& comm) {
    int flag;
    MPI_Initialized(&flag);

    const int rc = import_mpi4py();
    assert(rc==0);
    return std::make_shared<Communicator>(*PyMPIComm_Get(comm.ptr()));
  } ));
#endif
  commWrap.def("GetRank", &Communicator::GetRank);
  commWrap.def("GetSize", &Communicator::GetSize);
  commWrap.def("Barrier", &Communicator::Barrier);
  commWrap.def("Bcast", &Communicator::Bcast<py::EigenDRef<Eigen::VectorXd> >);
  commWrap.def("Bcast", &Communicator::Bcast<py::EigenDRef<Eigen::MatrixXd> >);
#if PARCER_HAS_MPI4PY
  //commWrap.def("GetComm", (pybind11::handle (Communicator::GetComm)()) &Communicator::GetComm, { return pybind11::handle(PyMPIComm_New(Communicator::GetComm())); } );
  commWrap.def("GetComm", &CallGetComm);
#endif
}


//.def("Gradient", (Eigen::VectorXd const& (ModPiece::*)(unsigned int, unsigned int, std::vector<Eigen::VectorXd> const&, Eigen::VectorXd const&)) &ModPiece::Gradient)
