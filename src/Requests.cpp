
#include "parcer/Requests.h"

#include <cassert>

using namespace parcer;

/*
bool parcer::Request::~Request()
{}
*/

bool Request::HasCompleted()
{
  int completedFlag;
  MPI_Test(&mpiRequest, &completedFlag, MPI_STATUS_IGNORE);
  return completedFlag==1;
}

void Request::WaitToFinish()
{
  std::cout << "(parcer) IN WAIT TO FINISH" << std::endl << std::flush;
  //int flag;
  //MPI_Initialized(&flag);
  //std::cout << "(parcer) FLAG: " << flag << std::endl << std::flush;
  int errCode = MPI_Wait(&mpiRequest, MPI_STATUS_IGNORE);
  assert(errCode==MPI_SUCCESS);
}

void Request::Clear()
{
  buffer.clear();
  msgSize = -1;
  mpiRequest = MPI_Request();
  msgType = MPI_DATATYPE_NULL;
}

int RecvRequest::Sender() const{
  return senderRank;
}

void RecvRequest::Clear(){
  Request::Clear();
  senderRank = -1;
}
