#/bin/bash

mkdir temp
cd temp

git clone --depth 1 -b master https://github.com/USCiLab/cereal.git

if [ -d "../cereal" ]; then
rm -r ../cereal
fi

cp -r cereal/include/cereal ../include/cereal/

cd ../
rm -rf temp
