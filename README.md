## Installation
To build from source:
1. Clone this repository
2. Configure with CMake:
    - `cd parcer`
    - `cmake -DCMAKE_INSTALL_PREFIX=/your/install/path ../`
3. Build the parcer library
    - `make install`
4. (Optional) Run the tests

### Build options

| CMAKE Variable | Description  |
| ------------- |:-------------:|
| `GTEST_ROOT`      | (Optional) The directory where the gtest `include` and `lib` folders are located (e.g., `/usr/local`)|
