#ifndef REQUESTS_H_
#define REQUESTS_H_

#include <mpi.h>

#include <sstream>
#include <vector>

#include <parcer/binary_buffer.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/utility.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/utility.hpp>

//#include <boost/core/demangle.hpp>

namespace parcer
{

  class Communicator;

  /**
    This class and it's member functions are involved in MPI requests.
  */

  class Request
  {

    friend class Communicator;

  public:

    /**
      @brief Constructor.
      @details Initializes msgSize to be 0.
    */

    Request() : msgSize(0){};

    /// Destructor
    virtual ~Request() = default;

    /**
      @brief Runs MPI_Test.
      @return completedFlag from MPI_Test.
    */

    bool HasCompleted();

    /**
      @brief Runs MPI_Wait and asserts the result of MPI_Wait is equal to
      MPI_SUCCESS.
    */

    void WaitToFinish();

    /**
      @brief Clears message buffer, sets msgSize equal to -1, instances
      MPI_Request, and sets the msgType to MPI_DATATYPE_NULL.
    */

    virtual void Clear();

    /**
      @brief Gets the MPI message size.
      @return MPI message size (msgSize).
    */

    int Size()
    {
      return msgSize;
    };

    //void CallParentTest() { std::cout << "CALLED REQUEST PARENT" << std::endl; }

  protected:

    /// MPI message buffer
    std::vector<char> buffer;

    /// MPI message size
    int msgSize;

    /// MPI_request object
    MPI_Request mpiRequest;

    /// MPI data type
    MPI_Datatype msgType;

  };

  /**
    This class and it's member functions are involved in MPI recieve requests.
  */

  class RecvRequest : public Request
  {

  public:

    /**
      @brief Constructor.
      @details Initializes senderRank to be -1.
    */

    RecvRequest() : senderRank(-1){};

    /// Destructor
    virtual ~RecvRequest() = default;

    /**
      @brief Returns the rank of the sender in the MPI recieve process.
      @return Member variable senderRank.
    */

    int Sender() const;

    /**
      @brief Calls the clear frunction from the Request class and sets the
      senderRank to be -1.
    */

    virtual void Clear() override;

    /**
      @brief Calls FillObject and extracts the data from the buffer.
      @return Message from the buffer.
    */

    template<typename objType>
    objType GetObject()
    {
      //std::cout << "(parcer) start" << std::endl << std::flush;
      /*{
        objType output;
        FillObject(output);
      }*/
      //std::cout << "(parcer) in get" << std::endl << std::flush;
      objType output;
      //std::cout << "(parcer) here" << std::endl << std::flush;
      FillObject(output);
      //std::cout << "(parcer) what" << std::endl << std::flush;
      //std::cout << "(parcer) " << boost::core::demangle(typeid(output).name()) << std::endl << std::endl << std::flush;
      return output;
      //return objType();
    }

    /**
      @brief Function that first waits to make sure the transfer is complete
      and deserializes the recived message.
    */

    template<typename objType>
    void FillObject(objType &obj)
    {
      //CallParentTest();
      //std::cout << "(parcer) FILL OBJECT" << std::endl << std::flush;
      int flag;
      MPI_Initialized(&flag);
      //std::cout << "(parcer) FLAG: " << flag << std::endl << std::flush;
      //std::cout << "(parcer) HERE" << std::endl << std::flush;
      //int errCode = MPI_Wait(&mpiRequest, MPI_STATUS_IGNORE);
      //assert(errCode==MPI_SUCCESS);
      //testcall();
      //std::cout << "(parcer) OKAY" << std::endl << std::flush;
      // first, wait to make sure the transfer is complete
      WaitToFinish();
      //std::cout << "(parcer) WAIT TO FINISH DONE" << std::endl << std::flush;

      //
      // Deserialize received message
      //
      {
        cereal::BinaryInputBuffer archive(buffer);
        //std::cout << "(PARCER) BEFORE ARC" << std::endl;
        archive(obj);
        //std::cout << "(PARCER) AFTER ARC" << std::endl;
      }

    }

    /*void testcall() {
      std::cout << "(parcer) IN TEST" << std::endl;
      int flag;
      MPI_Initialized(&flag);
      std::cout << "(parcer) FLAG: " << flag << std::endl << std::flush;
      int errCode = MPI_Wait(&mpiRequest, MPI_STATUS_IGNORE);
      assert(errCode==MPI_SUCCESS);
    }*/

    /// Rank of the process involved in the MPI send.
    int senderRank;

  };

  /**
    This class and it's member functions are involved in MPI send requests.
  */

  class SendRequest : public Request
  {

  public:

    /// Destructor
    virtual ~SendRequest() = default;

    /**
      @brief Serializes the passed object and sets the message size.
      @param[in] obj
    */

    template<typename objType>
    void BuildMessage(const objType& obj)
    {

      //
      // Serialize object
      //
      buffer.clear();

      {
	      cereal::BinaryOutputBuffer archive(buffer);
	      archive(obj);
      }

      msgSize = buffer.size();

    }

  };

} // namespace Communicator

#endif
