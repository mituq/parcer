#ifndef PARCER_ARCHIVES_BINARY_BUFFER_HPP_
#define PARCER_ARCHIVES_BINARY_BUFFER_HPP_

#include "cereal/cereal.hpp"
#include <sstream>
#include <iostream>

namespace cereal
{

  /**
    This class and it's member functions are used to interface with the Cereal
    OutputArchive. An output archive designed to save data in a compact binary
    representation.
  */

  class BinaryOutputBuffer : public OutputArchive<BinaryOutputBuffer, AllowEmptyClassElision>
  {
    public:

      /**
        @brief Constructor.
        @details Provides access to Cereal OutputArchive functions.
        @param[in] buffIn Output buffer.
      */

      BinaryOutputBuffer(std::vector<char>& buffIn) : OutputArchive<BinaryOutputBuffer, AllowEmptyClassElision>(this), buff(buffIn)
      {}

      /**
        @brief Destructor.
      */

      ~BinaryOutputBuffer() CEREAL_NOEXCEPT = default;

      /**
        @brief Writes size bytes of data to the output stream.
        @param[in] data Input data.
        @param[in] size Data size.
      */

      void saveBinary( const void * data, std::size_t size )
      {
        const char* data_bytes = reinterpret_cast<const char*>( data );

        for(int i=0; i<size; ++i)
            buff.push_back(data_bytes[i]);
      }

    private:

      /// Output buffer
      std::vector<char>& buff;
  };

  /**
  This class and it's member functions are used to interface with the Cereal
  InputArchive. A base input archive for all input archives.
  */

  class BinaryInputBuffer : public InputArchive<BinaryInputBuffer, AllowEmptyClassElision>
  {
    public:

      /**
        @brief Constructor.
        @details Provides access to Cereal InputArchive functions.
        @param[in] buffIn Input buffer.
      */

      BinaryInputBuffer(std::vector<char>& buffIn) : InputArchive<BinaryInputBuffer, AllowEmptyClassElision>(this), buff(buffIn){}

      /**
        @brief Destructor.
      */

      ~BinaryInputBuffer() CEREAL_NOEXCEPT = default;

      /**
        @brief Loads binary data and erases from buffer.
        @param[in] data Input data.
        @param[in] size Data size.
      */

      void loadBinary( void * const data, std::size_t size )
      {

        char* data_bytes = reinterpret_cast<char*>( data );

        for(int i=0; i<size; ++i){
          data_bytes[i] = buff.at(i);
        }

        buff.erase(buff.begin(), buff.begin()+size);

      }

    private:

      /// Input buffer
      std::vector<char>& buff;

  };

  // ######################################################################
  // Common BinaryBuffer serialization functions

  //! Saving for POD types to binary
  template<class T> inline
  typename std::enable_if<std::is_arithmetic<T>::value, void>::type
  CEREAL_SAVE_FUNCTION_NAME(BinaryOutputBuffer & ar, T const & t)
  {
    ar.saveBinary(std::addressof(t), sizeof(t));
  }

  //! Loading for POD types from binary
  template<class T> inline
  typename std::enable_if<std::is_arithmetic<T>::value, void>::type
  CEREAL_LOAD_FUNCTION_NAME(BinaryInputBuffer & ar, T & t)
  {
    ar.loadBinary(std::addressof(t), sizeof(t));
  }

  //! Serializing NVP types to binary
  template <class Archive, class T> inline
  CEREAL_ARCHIVE_RESTRICT(BinaryInputBuffer, BinaryOutputBuffer)
  CEREAL_SERIALIZE_FUNCTION_NAME( Archive & ar, NameValuePair<T> & t )
  {
    ar( t.value );
  }

  //! Serializing SizeTags to binary
  template <class Archive, class T> inline
  CEREAL_ARCHIVE_RESTRICT(BinaryInputBuffer, BinaryOutputBuffer)
  CEREAL_SERIALIZE_FUNCTION_NAME( Archive & ar, SizeTag<T> & t )
  {
    ar( t.size );
  }

  //! Saving binary data
  template <class T> inline
  void CEREAL_SAVE_FUNCTION_NAME(BinaryOutputBuffer & ar, BinaryData<T> const & bd)
  {
    ar.saveBinary( bd.data, static_cast<std::size_t>( bd.size ) );
  }

  //! Loading binary data
  template <class T> inline
  void CEREAL_LOAD_FUNCTION_NAME(BinaryInputBuffer & ar, BinaryData<T> & bd)
  {
    ar.loadBinary(bd.data, static_cast<std::size_t>(bd.size));
  }
} // namespace cereal

// register archives for polymorphic support
CEREAL_REGISTER_ARCHIVE(cereal::BinaryOutputBuffer)
CEREAL_REGISTER_ARCHIVE(cereal::BinaryInputBuffer)

// tie input and output archives together
CEREAL_SETUP_ARCHIVE_TRAITS(cereal::BinaryInputBuffer, cereal::BinaryOutputBuffer)

#endif // CEREAL_ARCHIVES_BINARY_HPP_
