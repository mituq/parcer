#ifndef PARCER_ALLCLASSWRAPPERS_H_
#define PARCER_ALLCLASSWRAPPERS_H_

#include <pybind11/pybind11.h>

namespace parcer {
  namespace PythonBindings {
    void CommunicatorWrapper(pybind11::module &m);
  }
}

#endif
