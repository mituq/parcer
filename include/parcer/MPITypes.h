#ifndef MPITYPES_H_
#define MPITYPES_H_

#include <mpi.h>

namespace parcer
{
    /// Struct with one function that returns the MPI type.
    template<typename scalarType>
    struct MPI_Type {
      /// Gets MPI data type
      /// @return Null data type
	    static MPI_Datatype GetFlag() { return MPI_DATATYPE_NULL; }
    };

    /// Struct with one function that returns the MPI type.
    template<>
    struct MPI_Type<bool> {
      /// Gets MPI data type
      /// @return Bool data type
      static MPI_Datatype GetFlag() { return MPI_C_BOOL; }
    };

    /// Struct with one function that returns the MPI type.
    template<>
    struct MPI_Type<double> {
      /// Gets MPI data type
      /// @return Double data type
      static MPI_Datatype GetFlag() { return MPI_DOUBLE; }
    };

    /// Struct with one function that returns the MPI type.
    template<>
    struct MPI_Type<char> {
      /// Gets MPI data type
      /// @return Char data type
      static MPI_Datatype GetFlag() { return MPI_CHAR; }
    };

    /// Struct with one function that returns the MPI type.
    template<>
    struct MPI_Type<short> {
      /// Gets MPI data type
      /// @return Short data type
      static MPI_Datatype GetFlag() { return MPI_SHORT; }
    };

    /// Struct with one function that returns the MPI type.
    template<>
    struct MPI_Type<int> {
      /// Gets MPI data type
      /// @return Int data type
      static MPI_Datatype GetFlag() { return MPI_INT; }
    };

    /// Struct with one function that returns the MPI type.
    template<>
    struct MPI_Type<long> {
      /// Gets MPI data type
      /// @return Long data type
      static MPI_Datatype GetFlag() { return MPI_LONG; }
    };

    /// Struct with one function that returns the MPI type.
    template<>
    struct MPI_Type<float> {
      /// Gets MPI data type
      /// @return Float data type
      static MPI_Datatype GetFlag() { return MPI_FLOAT; }
    };

    /// Struct with one function that returns the MPI type.
    template<>
    struct MPI_Type<unsigned short> {
      /// Gets MPI data type
      /// @return Unsigned short data type
      static MPI_Datatype GetFlag() { return MPI_UNSIGNED_SHORT; }
    };

    /// Struct with one function that returns the MPI type.
    template<>
    struct MPI_Type<unsigned int> {
      /// Gets MPI data type
      /// @return Unsigned data type
      static MPI_Datatype GetFlag() { return MPI_UNSIGNED; }
    };

    /// Struct with one function that returns the MPI type.
    template<>
    struct MPI_Type<unsigned long> {
      /// Gets MPI data type
      /// @return Unsigned long data type
      static MPI_Datatype GetFlag() { return MPI_UNSIGNED_LONG; }
    };
}

#endif // MPITYPES_H_
