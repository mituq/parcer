#ifndef QUEUE_H
#define QUEUE_H

#include "parcer/Communicator.h"

#include <cereal/types/vector.hpp>

#include <assert.h>
#include <memory>
#include <deque>
#include <unordered_map>

#include <atomic>
#include <mutex>
#include <thread>

namespace parcer{

  /**
    This struct sets up the MPI work type to be done.
  */

  template<typename ObjType>
  struct WorkType
  {

    /**
      @brief Constructor.
      @details Stores work type with MPI identifier and object type. Member
      function can serialize passed object.
      @param[in] idIn MPI identifier
      @param[in] xIn Object type
    */

    WorkType(unsigned idIn=0, ObjType const& xIn=ObjType()) : id(idIn), x(xIn){};

    /// Work type id.
    unsigned id;

    /// Object type.
    ObjType x;

    /**
      @brief Serialize things by passing them to the archive.
      @param[in] archive Cereal archive object.
    */

    template<class Archive>
    void serialize(Archive & archive)
    {
      archive( id, x );
    }

  }; // End struct WorkType

  /**
    @brief This class sets up the MPI queue.
    @details This class defines a first-in-first-out (FIFO) queue for an arbitrary, but constant,
    workload.  Note that the the destructor of this class must be called before the
    MPI_Finalize method, or else a segfault will occur.
  */
  template<typename InputType, typename OutputType, typename GenericWorkType, OutputType (GenericWorkType::*EvalMethod)(InputType) = &GenericWorkType::Evaluate>
  class Queue {

  public:

    /**
      @brief Constructor.
      @details Construct the queue. This constructor will trap all processes
      except the master process (rank==0).
      @param[in] workIn Work to be done.
      @param[in] commIn MPI communicator.
    */

    Queue(std::shared_ptr<GenericWorkType> workIn,
          std::shared_ptr<Communicator>    commIn) : currId(0),
                                                     comm(commIn),
                                                     work(workIn),
                                                     numItems(0),
                                                     keepWorking(true)
    {
      // Get the rank
      mpiRank = comm->GetRank();

      // Get the total number of processes and make sure there are at least 3 processes
      mpiSize = comm->GetSize();

      assert(mpiSize>1);

      if(mpiRank==0){

        // Spawn a new thread on the master process to function as a coordinator
        coordThread = std::make_shared<std::thread>(&Queue::CoordinatorLoop, this);

      }else{

        WorkerLoop();

      }

    };

    /// Destructor.
    virtual ~Queue()
    {
      if(mpiRank==0)
      {
        keepWorking = false;
        coordThread->join();
        Release();
      }

    };

    /**
      @brief Submits a point to evaluate.
      @param[in] x The point where we want to perform the work.
      @return A unique id that identifies this point in the queue and can be
      used in GetResult.
    */

    unsigned SubmitWork(InputType const& x)
    {
      assert(mpiRank==0);

      {
        std::lock_guard<std::mutex> guard(completedMutex);
        workStatus[currId] = false;
      }

      // put result into "todo" deque
      {
        std::lock_guard<std::mutex> guard(todoMutex);

        workToDo.emplace_back(currId, x);
        numItems++;
      }

      currId++;
      return currId-1;
    }


    /**
      @brief Return the function evaluation for a submitted point.
      @param[in] id The identifier returned from "SubmitWork".
      @return The function evaluation f(x).
    */

    OutputType GetResult(unsigned id)
    {
      assert(mpiRank==0);

      // Just hang out until the work is done
      OutputType result;

      while( !workStatus.at(id) ) {}

      completedMutex.lock();

      result = workCompleted.at(id);

      workCompleted.erase(id);
      
      workStatus.erase(id);

      completedMutex.unlock();

      return result;
    }

    /**
      @brief Function is currently empty. Do not use.
      @param[in] id The identifier returned from "SubmitWork".
      @return Supposed to return a bool but currently no return statement.
    */

    bool CheckStatus(unsigned id){

      // Should there be something here?
      throw std::runtime_error("ParCER error: CheckStatus call not supported here!\n");
    }

  private:

    void WorkerLoop()
    {
      // Set up a release receive so the master can tell us when we're done
      RecvRequest continueRequest;
      int releaseComplete;
      bool cont = true;
      comm->Irecv(cont, masterRank, releaseTag, continueRequest);

      // Wait for work
      WorkType<InputType> workInput;
      RecvRequest workRequest;
      bool workExists = false;
      //mpiErr = comm->Irecv(workInput, sizeof(WorkType), MPI_BYTE, masterRank, workToDoTag, comm, &workRequest);
      //assert(mpiErr == MPI_SUCCESS);

      // Send a message to the coordinator indicating we're ready for work
      comm->Send(mpiRank, masterRank, needWorkTag);

      while(true)
      {

        //mpiErr = MPI_Test(&continueRequest, &releaseComplete, MPI_STATUS_IGNORE);
        //assert(mpiErr == MPI_SUCCESS);

        if(continueRequest.HasCompleted())
        {
          if(!cont)
            break;
        }

        workExists = comm->Iprobe(masterRank, workToDoTag, workRequest);

        if(workExists)
        {


          //std::cout << "Worker: Receiving work with id " << workInput.id << std::endl;
          WorkType<InputType> workInput = comm->Recv<WorkType<InputType>>(masterRank, workToDoTag);

          //std::cout << "Working on item " << workInput.id << "... " << std::flush;
          // Evaluate the work
          WorkType<OutputType> workOutput;
          workOutput.id = workInput.id;
          workOutput.x = ((*work).*EvalMethod)(workInput.x);

          // Send the work back to master
          comm->Send(workOutput, masterRank, completedTag);

          //std::cout << "done" << std::endl;

          // Get ready for more work
          workRequest.Clear();
          //mpiErr = MPI_Irecv(&workInput, sizeof(WorkType), MPI_BYTE, masterRank, workToDoTag, comm, &workRequest);
          //assert(mpiErr == MPI_SUCCESS);

          //std::cout << "Worker: Sending results for with id " << workInput.id << "... " << std::flush;

          // Send a message to the coordinator indicating we're ready for work
          comm->Send(mpiRank, masterRank, needWorkTag); //MPI_Send(&mpiRank, 1, MPI_INT, masterRank, needWorkTag, comm);

          //std::cout << "done" << std::endl;
        }

      }

    } // End of WorkerLoop

    void CoordinatorLoop()
    {
      // Receive a message from any source that is asking for work
      RecvRequest workRequest;
      //int mpiErr = MPI_Irecv(&workerReadyRank, 1, MPI_INT, MPI_ANY_SOURCE, needWorkTag, comm, &workRequest);
      //assert(mpiErr == MPI_SUCCESS);

      // Receive a message from any source that has completed work
      WorkType<OutputType> completedWork;
      RecvRequest completeRequest;
      //mpiErr = MPI_Irecv(&completedWork, sizeof(WorkType), MPI_BYTE, MPI_ANY_SOURCE, completedTag, comm, &completeRequest);
      //assert(mpiErr == MPI_SUCCESS);

      while(keepWorking)
      {

        // Look for messages with completed work
        bool resultsReady = comm->Iprobe(MPI_ANY_SOURCE, completedTag, completeRequest);

        if(resultsReady)
        {

          completedWork = comm->Recv<WorkType<OutputType>>(completeRequest.Sender(), completedTag);

          {
            std::lock_guard<std::mutex> locker(completedMutex);
            workCompleted[completedWork.id] = completedWork.x;
            workStatus.at(completedWork.id) = true;
          }

          completeRequest.Clear();
        }

        // Look for workers that need work
        bool workersReady = comm->Iprobe(MPI_ANY_SOURCE, needWorkTag, workRequest);

        if(workersReady && (numItems>0))
        {

          int workerReadyRank = comm->Recv<int>(workRequest.Sender(), needWorkTag);

          {
            std::lock_guard<std::mutex> locker(todoMutex);
            comm->Send(workToDo.at(0), workerReadyRank, workToDoTag);
            workToDo.pop_front();
            numItems--;
          }

          workRequest.Clear();

        }

      }

    } // end of CoordinatorLoop


    void Release()
    {
      bool falseMsg = false;

      for(int i=1; i<mpiSize; i++)
        comm->Send(falseMsg, i, releaseTag);

    }


    std::unordered_map<unsigned, OutputType> workCompleted;
    std::unordered_map<unsigned, bool> workStatus;

    unsigned currId;

    std::shared_ptr<Communicator> comm;
    std::shared_ptr<GenericWorkType> work;

    std::deque<WorkType<InputType>> workToDo;
    std::atomic_int numItems; // size of worktodo




    int mpiRank, mpiSize; // rank and communicator size


    std::shared_ptr<std::thread> coordThread;
    std::mutex todoMutex, completedMutex, printMutex;

    // Special ranks
    const int masterRank = 0;

    // Message tags
    const int needWorkTag = 1;
    const int releaseTag = 2;
    const int workToDoTag = 3;
    const int completedTag = 4;

    std::atomic_bool keepWorking;

  }; // class Queue

} // namespace ParQueue


#endif
