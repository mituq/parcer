#ifndef COMMUNICATOR_H_
#define COMMUNICATOR_H_

#include <mpi.h>

#include <string>
#include <sstream>

#include <parcer/binary_buffer.hpp>

#include "ParCerConfig.h"

#include "parcer/MPITypes.h"
#include "parcer/Requests.h"

namespace parcer
{

  /**
    This class and it's member functions provide access to MPI exception
    messages.
  */

  class mpiException : std::exception {

  public:

    /**
      @brief Constructor.
      @details Provides access to MPI message information.
      @param[in] msgIn MPI message.
    */

    mpiException(std::string const& msgIn) : msg(msgIn){};

    /**
      @brief Get the MPI exception message.
      @return MPI exception message.
    */

    const char* what() const throw ()
    {
      return msg.c_str();
    };

  private:

    /// MPI message string.
    std::string msg;
  };

  /**
    This class and it's member functions are used in MPI communication.
  */

  class Communicator {

  public:

    /**
      @brief Constructor.
      @details Calls the protected constructor that is instanced with an MPI
      communicator object.
    */
    Communicator() {
      int rank;
      MPI_Comm_rank(MPI_COMM_WORLD, &rank);

      MPI_Comm_split(MPI_COMM_WORLD, 0, rank, &comm);
    }

    /**
    Splits MPI_COMM_WORLD using MPI_Comm_split
    @param[in] color The color for this rank
    @param[in] key The key for this rank
    */
    Communicator(int const color, int const key) {
      MPI_Comm_split(MPI_COMM_WORLD, color, key, &comm);
    }

    /**
      @brief Constructor.
      @details Gets MPI communication information.
      @param[in] otherComm MPI communication object.
    */
    Communicator(MPI_Comm const& otherComm) : comm(otherComm) {};

    virtual ~Communicator() = default;

    inline void FreeCommunicator() {
      if( comm && comm!=MPI_COMM_SELF && comm!=MPI_COMM_WORLD ) {
        MPI_Comm_free(&comm);
      }
    }

    const MPI_Comm& GetMPICommunicator() const {
      return comm;
    }

    /**
       @brief Blocks until all processors catchup
     */
    void Barrier() const { MPI_Barrier(comm); }

    /**
      @brief Gets the rank of a process in this communicator by calling
      MPI_Comm_rank.
      @return Rank of the MPI process.
    */

    int GetRank() const
    {
      int rank;
      int mpiErr = MPI_Comm_rank(comm, &rank);

      if(mpiErr != MPI_SUCCESS)
        throw mpiException("Unsuccessful call to MPI_Comm_rank in parcer::Communicator::GetRank().");

      return rank;
    }

    /**
      @brief Gets the number of processes in this communicator by calling
      MPI_Comm_size.
      @return Number of MPI processes.
    */
    int GetSize() const
    {
      int size;
      int mpiErr = MPI_Comm_size(comm, &size);

      if(mpiErr != MPI_SUCCESS)
        throw mpiException("Unsuccessful call to MPI_Comm_Size in parcer::Communicator::GetSize().");

      return size;
    }

    /**
       @brief Used to broadcast data to all processors
     */
    template<typename ObjType>
      void Bcast(ObjType& obj, int root) {
      if( std::is_arithmetic<ObjType>::value ) {
	int mpiErr = MPI_Bcast(&obj, 1, MPI_Type<ObjType>::GetFlag(), root, comm);

	if( mpiErr!=MPI_SUCCESS ) {
          throw mpiException("Unsuccessful call to MPI_Bcast for simple type in parcer::Communicator::Bcast. Bcast failed for root " + std::to_string(root));
	}
      } else {
	std::vector<char> s;
	{ // serialize object
	  cereal::BinaryOutputBuffer archive(s);
	  archive(obj);
	}

	// get the size of the message
	int msg_size = s.size();
	int mpiErr = MPI_Bcast(&msg_size, 1, MPI_INT, root, comm);
	if( mpiErr!=MPI_SUCCESS ) {
          throw mpiException("Unsuccessful call to MPI_Bcast for simple type in parcer::Communicator::Bcast. Bcast failed for root " + std::to_string(root));
	}

	// send the message
	if( GetRank()!=root ) { s.resize(msg_size); }
	mpiErr = MPI_Bcast(s.data(), s.size(), MPI_CHAR, root, comm);
	if( mpiErr!=MPI_SUCCESS ) {
          throw mpiException("Unsuccessful call to MPI_Bcast for simple type in parcer::Communicator::Bcast. Bcast failed for root " + std::to_string(root));
	}

	{ // deserialize object
	  cereal::BinaryInputBuffer archive(s);
	  archive(obj);
	}
      }
    }

    /**
      @brief Used to begin the nonblocking MPI send operation.
      @details If the sent object is of type arithmetic the function calls
      MPI_Isend and tests if call is successful. If the object type is not
      arithmetic the object is passed to an instance of type
      cereal::BinaryOutputBuffer to serialize and Isend is called again.
      @param[in] obj A generic object to be sent by MPI_Isend.
      @param[in] dest MPI rank of destination.
      @param[in] rawTag A message tag.
      @param[in] request MPI communication request.
    */

    template<typename ObjType>
    void Isend(ObjType const& obj, int dest, int rawTag, SendRequest& request)
    {
      int mpiErr;

      if(std::is_arithmetic<ObjType>::value){

        int mpiErr = MPI_Isend(&obj, 1, MPI_Type<ObjType>::GetFlag(), dest, rawTag, comm, &request.mpiRequest);

        if(mpiErr != MPI_SUCCESS)
          throw mpiException("Unsuccessful call to MPI_Isend for simple type in parcer::Communicator::Isend. Isend failed for destination " + std::to_string(dest) + " and tag " + std::to_string(rawTag));

      }else{

        request.buffer.clear();
        {
  	      cereal::BinaryOutputBuffer archive(request.buffer);
  	      archive(obj);
        }

        request.msgSize = request.buffer.size();

        Isend(dest, rawTag, request);
      }

    }

    /**
      @brief Used to begin the nonblocking MPI send operation.
      @details Calls the function MPI_Isend and tests if call is successful.
      Object to send is stored in the request instance.
      @param[in] dest MPI rank of destination.
      @param[in] rawTag A message tag.
      @param[in] request Is a SendRequest object with access to send buffer
      address, send buffer size, and communication request.
    */

    void Isend(int dest, int rawTag, SendRequest& request)
    {

      int ierr = MPI_Isend(request.buffer.data(),
                           request.msgSize,
                           MPI_CHAR,
                           dest,
                           rawTag,
                           comm,
                           &request.mpiRequest);

      if(ierr != MPI_SUCCESS)
        throw mpiException("Unsuccessful call to MPI_ISend in parcer::Communicator::Isend. Isend failed for destination " + std::to_string(dest) + " and tag " + std::to_string(rawTag));

    }

    /**
      @brief Starts a standard-mode, nonblocking receive.
      @details Calls MPI_Irecv and tests if call is successful.
      @param[in] obj Initial address of receive buffer.
      @param[in] source Rank of source.
      @param[in] rawTag Message tag.
      @param[in] request Communication request.
    */

    template<typename SimpleType, typename = typename std::enable_if<std::is_arithmetic<SimpleType>::value, SimpleType>::type>
    void Irecv(SimpleType& obj, int source, int rawTag, RecvRequest& request)
    {
      int mpiErr = MPI_Irecv(&obj, 1, MPI_Type<SimpleType>::GetFlag(), source, rawTag, comm, &request.mpiRequest);

      if(mpiErr != MPI_SUCCESS)
        throw mpiException("Unsuccessful call to MPI_Recv for basic type in parcer::Communicator::Irecv. Irecv failed for source " + std::to_string(source) + " and tag " + std::to_string(rawTag));

    }

    /**
      @brief Starts a standard-mode, nonblocking receive.
      @details Calls MPI_Irecv and tests if call is successful.
      @param[in] source Rank of source.
      @param[in] rawTag Message tag.
      @param[in] request Is a SendRequest object with access to send buffer
      address, send buffer size, and communication request.
    */

    void Irecv(int source, int rawTag, RecvRequest& request)
    {
      int flag;
      MPI_Initialized(&flag);
      int ierr = MPI_Irecv(request.buffer.data(),
                           request.msgSize,
                           MPI_CHAR,
                           source,
                           rawTag,
                           comm,
                           &request.mpiRequest);

      if(ierr != MPI_SUCCESS)
        throw mpiException("Unsuccessful call to MPI_Irecv in parcer::Communicator::Irecv.  Failed for source " + std::to_string(source) + " and tag " + std::to_string(rawTag));


    }

    /**
      @brief Nonblocking test for a message.
      @details Calls MPI_Iprobe and tests is call is successful. If message with
      a specified source, tag, and communicator is available the function
      MPI_Get_count is called, tested if succesful, and request buffer and
      sender rank is updated.
      @param[in] source Source rank.
      @param[in] rawTag Tag value.
      @param[in] request A RecvRequest object used in MPI_Get_count function.
      @return MPI flag. True if specified source, tag, and communicator is
      available.
    */

    bool Iprobe(int source, int rawTag, RecvRequest& request)
    {
      int flag = 0;

      MPI_Status probe_status;

      int ierr = MPI_Iprobe(source, rawTag, comm, &flag, &probe_status);

      if(ierr != MPI_SUCCESS)
        throw mpiException("Unsuccessful call to MPI_Iprobe in parcer::Communicator::Iprobe");


      if (flag)
      {
	      ierr = MPI_Get_count(&probe_status, MPI_CHAR, &request.msgSize);

        if(ierr != MPI_SUCCESS)
          throw mpiException("Unsuccessful call to MPI_Get_count in parcer::Communicator::Iprobe");

	      request.buffer = std::vector<char>(request.msgSize);

        request.senderRank = probe_status.MPI_SOURCE;
        //std::cout << request.senderRank << " " << probe_status.MPI_SOURCE << std::endl;
        //assert(request.senderRank==source);
      }

      return flag;
    }

    /**
      @brief Performs a blocking send.
      @details If ObjType is of type arithmetic MPI_Send is called and tested
      if call is succesful. If ObjType is not arithmetic, object is serialized
      and MPI_Send is called again and tested if succesful.
      @param[in] obj Send buffer.
      @param[in] dest rank of destination.
      @param[in] rawTag Message tag.
    */

    template<typename ObjType>
    void Send(ObjType const& obj, int dest, int rawTag)
    {

      if(std::is_arithmetic<ObjType>::value){
        int ierr = MPI_Send(&obj, 1, MPI_Type<ObjType>::GetFlag(), dest, rawTag, comm);

        if(ierr != MPI_SUCCESS)
          throw mpiException("Unsuccessful call to MPI_Send with simple type in parcer::Communicator::Send");

      }else{

        //
        // Serialize object
        //
        std::vector<char> s;
        {
  	      cereal::BinaryOutputBuffer archive(s);
  	      archive(obj);
        }

        int ierr = MPI_Send(s.data(), s.size(), MPI_CHAR, dest, rawTag, comm);
        if(ierr != MPI_SUCCESS)
          throw mpiException("Unsuccessful call to MPI_Send in parcer::Communicator::Send");

      }

    }

    /**
     * @brief Performs a blocking synchronous send.
     * @details If ObjType is of type arithmetic MPI_Ssend is called and tested
     * if call is succesful. If ObjType is not arithmetic, object is serialized
     * and MPI_Send is called again and tested if succesful.
     * @param[in] obj Send buffer.
     * @param[in] dest rank of destination.
     * @param[in] rawTag Message tag.
     */

    template<typename ObjType>
    void Ssend(ObjType const& obj, int dest, int rawTag)
    {

      if(std::is_arithmetic<ObjType>::value){
        int ierr = MPI_Ssend(&obj, 1, MPI_Type<ObjType>::GetFlag(), dest, rawTag, comm);

        if(ierr != MPI_SUCCESS)
          throw mpiException("Unsuccessful call to MPI_Ssend with simple type in parcer::Communicator::Ssend");

      }else{

        //
        // Serialize object
        //
        std::vector<char> s;
        {
          cereal::BinaryOutputBuffer archive(s);
          archive(obj);
        }

        int ierr = MPI_Ssend(s.data(), s.size(), MPI_CHAR, dest, rawTag, comm);
        if(ierr != MPI_SUCCESS)
          throw mpiException("Unsuccessful call to MPI_Ssend in parcer::Communicator::Ssend");

      }

    }

    /**
      @brief Blocking receive for a message.
      @details If ObjType is of type arithmetic MPI_Recv is called and tested
      if call is succesful. If ObjType is not arithmetic, MPI_Probe,
      MPI_Get_count, and MPI_Recv are called in succession. The received
      message is then deserialized.
      @param[in] source Rank of source.
      @param[in] rawTag Message tag.
      @param[in] status MPI status object (optional). Defaults to
      MPI_STATUS_IGNORE.
      @return Address of receive buffer.
    */

    template<typename ObjType>
    ObjType Recv(int source, int rawTag, MPI_Status* status = MPI_STATUS_IGNORE)
    {
      ObjType output;

      if(std::is_arithmetic<ObjType>::value){
        int mpiErr = MPI_Recv(&output, 1, MPI_Type<ObjType>::GetFlag(), source, rawTag, comm, status);

        if(mpiErr != MPI_SUCCESS)
          throw mpiException("Unsuccessful call to MPI_Recv for simple type in parcer::Communicator::Recv.");

        return output;

      }else{

        int msg_size;
        MPI_Status probe_status;

        int ierr = MPI_Probe(source, rawTag, comm, &probe_status);
        if(ierr != MPI_SUCCESS)
            throw mpiException("Unsuccessful call to MPI_Probe in parcer::Communicator::Recv");

        ierr = MPI_Get_count(&probe_status, MPI_CHAR, &msg_size);
        if(ierr != MPI_SUCCESS)
            throw mpiException("Unsuccessful call to MPI_Get_count in parcer::Communicator::Recv");

        std::vector<char> char_vec(msg_size);
        ierr = MPI_Recv(char_vec.data(), msg_size, MPI_CHAR, source, rawTag, comm, status);

        if(ierr != MPI_SUCCESS)
            throw mpiException("Unsuccessful call to MPI_Recv in parcer::Communicator::Recv");

        // Deserialize received message
        {
  	      cereal::BinaryInputBuffer archive(char_vec);
  	      archive(output);
        }

        return output;
      }
    }

    inline MPI_Comm GetComm() const { return comm; }

  protected:

    /// MPI communication object
    MPI_Comm comm;

  };

} // namespace parcer


#endif
