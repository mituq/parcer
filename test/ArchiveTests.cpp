#include "gtest/gtest.h"

#include <vector>

#include "parcer/binary_buffer.hpp"

struct TypeToSave
{
    TypeToSave() : TypeToSave(0.0, 0){};

    TypeToSave(double aIn, unsigned bIn) : a(aIn), b(bIn){};

    double a;
    unsigned b;

    template<class Archive>
    void serialize(Archive & archive)
    {
      archive(a, b); // serialize things by passing them to the archive
    }
};

TEST(ArchiveTests, SaveLoad)
{
  TypeToSave obj(1.0,2);

  std::vector<char> buffer;
  {
    cereal::BinaryOutputBuffer archive(buffer);
    archive(obj);
  }

  TypeToSave obj2;
  {
    cereal::BinaryInputBuffer archive(buffer);
    archive(obj2);
  }

  EXPECT_EQ(obj.a, obj2.a);
  EXPECT_EQ(obj.b, obj2.b);
}
