#include "gtest/gtest.h"
#include "mpi.h"

#include <vector>
#include <memory>

#include "parcer/Queue.h"

using namespace parcer;

class Work
{
public:
  Work(double aIn) : a(aIn){};

  double Evaluate(double x) {
    //sleep(1);
    return a*x*x;
  };

    double AnotherEvaluate(double x) {
    return a*a*x+x*x-a;
  };

private:
  const double a;
};


TEST(QueueTests, DoubleEvaluation) {
  // Get the rank of the current process
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // Set up the work to perform (this is done on all processes)
  std::shared_ptr<Work> work = std::make_shared<Work>(2.0);

  {
    // Set up the Queue.  This will "capture" all processes but the master (rank=0)
    auto comm = std::make_shared<Communicator>();

    // Create the queue.  Template parameters are <InputType, OutputType, WorkType>
    Queue<double, double, Work> queue(work, comm);

    //Use the master process to submit jobs.
    if(rank==0){
      unsigned numEvals = 100;
      std::vector<unsigned> ptIds(numEvals);
      std::vector<double> evalPts(numEvals);

      // Submit all the work to the queue
      for(unsigned i=0; i<numEvals; ++i){
        evalPts.at(i) = double(i)/double(numEvals-1);
        ptIds.at(i) = queue.SubmitWork(evalPts.at(i));
      }

      // Retrieve all the completed evaluations. Each call will hang until that evaluation is available
      for(unsigned i=0; i<numEvals; ++i){
        double eval = queue.GetResult(ptIds.at(i));
        EXPECT_DOUBLE_EQ(work->Evaluate(evalPts.at(i)), eval);
      }

    }
  }
}

TEST(QueueTests, DoubleAnotherEvaluation) {
  // get the rank of the current process
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // set up the work to perform (this is done on all processes)
  std::shared_ptr<Work> work = std::make_shared<Work>(2.0);

  {
    // set up the Queue
    auto comm = std::make_shared<Communicator>();

    // create the queue.
    Queue<double, double, Work, &Work::AnotherEvaluate> queue(work, comm);

    // submit jobs.
    if(rank==0){
      unsigned numEvals = 100;
      std::vector<unsigned> ptIds(numEvals);
      std::vector<double> evalPts(numEvals);

      // submit work to the queue
      for(unsigned i=0; i<numEvals; ++i){
        evalPts.at(i) = double(i)/double(numEvals-1);
        ptIds.at(i) = queue.SubmitWork(evalPts.at(i));
      }

      // retrieve the completed evaluations
      for(unsigned i=0; i<numEvals; ++i){
        double eval = queue.GetResult(ptIds.at(i));
        EXPECT_DOUBLE_EQ(work->AnotherEvaluate(evalPts.at(i)), eval);
      }
    }
  }
}

TEST(QueueTests, TwoQueuesTest) {
  // get the rank of the current process
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // set up the work to perform (this is done on all processes)
  std::shared_ptr<Work> work = std::make_shared<Work>(2.0);

  // set up the communicator
  auto comm = std::make_shared<Communicator>();

  const unsigned numEvals = 100;
  
  for( unsigned int i=0; i<100; ++i ) {
    { // do the FIRST queue
      // create the queue
      Queue<double, double, Work> queue(work, comm);
      //auto queue = std::make_shared<Queue<double, double, Work> > (work, comm);
      
      // use the master process to submit jobs.
      if( rank==0 ) {
	std::vector<unsigned> ptIds(numEvals);
	std::vector<double> evalPts(numEvals);
	
	// Submit all the work to the queue
	for(unsigned i=0; i<numEvals; ++i){
	  evalPts.at(i) = double(i)/double(numEvals-1);
	  ptIds.at(i) = queue.SubmitWork(evalPts.at(i));
	}

	// Retrieve all the completed evaluations. Each call will hang until that evaluation is available
	for(unsigned i=0; i<numEvals; ++i){
	  double eval = queue.GetResult(ptIds.at(i));
	  EXPECT_DOUBLE_EQ(work->Evaluate(evalPts.at(i)), eval);
	}
      }
    } // end FIRST queue
      
    { // do the SECOND queue
      // create the queue
      Queue<double, double, Work, &Work::AnotherEvaluate> queue(work, comm);
      
      // use the master process to submit jobs.
      if( rank==0 ) {
	std::vector<unsigned> ptIds(numEvals);
	std::vector<double> evalPts(numEvals);
	
	// Submit all the work to the queue
	for(unsigned i=0; i<numEvals; ++i){
	  evalPts.at(i) = double(i)/double(numEvals-1);
	  ptIds.at(i) = queue.SubmitWork(evalPts.at(i));
	}

	// Retrieve all the completed evaluations. Each call will hang until that evaluation is available
	for(unsigned i=0; i<numEvals; ++i){
	  double eval = queue.GetResult(ptIds.at(i));
	  EXPECT_DOUBLE_EQ(work->AnotherEvaluate(evalPts.at(i)), eval);
	}
      }
    } // end SECOND queue
  } // end for loop
} // end test
