
#include "gtest/gtest.h"
#include "mpi.h"

#include <vector>
#include <memory>

#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/memory.hpp>

#include "parcer/Communicator.h"

using namespace parcer;


// This struct stores data to test this serialization and
// MPI helper library.
struct Content
{

  double x;
  std::vector<double> v;
  std::shared_ptr<Content> obj1;
  std::shared_ptr<Content> obj2;

  Content()
  {

    x = 0.0;

    v.push_back(0.0);
    v.push_back(0.0);
    v.push_back(0.0);

    obj1 = nullptr;
    obj2 = nullptr;

  }

  Content(double x_in,
	  double v0_in, double v1_in, double v2_in)
  {

    x = x_in;

    v.push_back(v0_in);
    v.push_back(v1_in);
    v.push_back(v2_in);

    obj1 = nullptr;
    obj2 = nullptr;

  }

  Content(double x_in,
	  double v0_in, double v1_in, double v2_in,
	  std::shared_ptr<Content> obj1_in,
	  std::shared_ptr<Content> obj2_in)
  {

    x = x_in;

    v.push_back(v0_in);
    v.push_back(v1_in);
    v.push_back(v2_in);

    obj1 = obj1_in;
    obj2 = obj2_in;

  }

  void Print()
  {

    std::cout << "x: " << x << std::endl;
    std::cout << "v: " << v[0] << "," << v[1] << "," << v[2] << std::endl;

    if (obj1 != nullptr)
    {
      std::cout << "Sub content obj1: " << std::endl;
      obj1->Print();
    }

    if (obj2 != nullptr)
    {
      std::cout << "Sub content obj2: " << std::endl;
      obj2->Print();
    }

  }

  template<class Archive>
  void serialize(Archive& archive)
  {

    archive(x, v, obj1, obj2);

  }

};

// SendRecvTest
// ------------
// Purpose: To test that any object is passed correctly from one process to
//          another. This includes the underlying objects that are pointed to
//          by the object being sent.
//
// We set up 3 levels of a Content object. Where the top level, i.e., level 2,
// points to level 1 and level 0, and the level 1 Content object also points to
// level 0.
//
TEST(CommunicatorTests, SendRecvTest)
{

  // Set up object to send and receive
  std::shared_ptr<Content> obj_level0
      = std::make_shared<Content>(1.0, 2.0, 3.0, 4.0);

  std::shared_ptr<Content> obj_level1
      = std::make_shared<Content>(1.1, 2.1, 3.1, 4.1, obj_level0, nullptr);

  Content obj_level2 = Content(1.2, 2.2, 3.2, 4.2, obj_level0, obj_level1);

  // Initialize MPI
  int ierr;

  // Instatiate communicator
  Communicator comm;

  int world_size = comm.GetSize();

  // Get the rank of the process
  int world_rank = comm.GetRank();


  int tag = 12;

  // Send operation
  if (world_rank==0)
  {
    comm.Send(obj_level2, 1, tag);
  }
  // Receive operation
  else if (world_rank==1)
  {

    Content obj_recv = comm.Recv<Content>(0, tag);

    // Test contents
    // -------------
    // Level 0
    EXPECT_EQ(obj_recv.obj1->x, obj_level0->x);

    EXPECT_EQ(obj_recv.obj1->v[0], obj_level0->v[0]);
    EXPECT_EQ(obj_recv.obj1->v[1], obj_level0->v[1]);
    EXPECT_EQ(obj_recv.obj1->v[2], obj_level0->v[2]);

    EXPECT_EQ(obj_recv.obj2->obj1->x, obj_level0->x);

    EXPECT_EQ(obj_recv.obj2->obj1->v[0], obj_level0->v[0]);
    EXPECT_EQ(obj_recv.obj2->obj1->v[1], obj_level0->v[1]);
    EXPECT_EQ(obj_recv.obj2->obj1->v[2], obj_level0->v[2]);

    // Level 1
    EXPECT_EQ(obj_recv.obj2->x, obj_level1->x);

    EXPECT_EQ(obj_recv.obj2->v[0], obj_level1->v[0]);
    EXPECT_EQ(obj_recv.obj2->v[1], obj_level1->v[1]);
    EXPECT_EQ(obj_recv.obj2->v[2], obj_level1->v[2]);

    // Level 2, i.e., top level
    EXPECT_EQ(obj_recv.x, obj_level2.x);

    EXPECT_EQ(obj_recv.v[0], obj_level2.v[0]);
    EXPECT_EQ(obj_recv.v[1], obj_level2.v[1]);
    EXPECT_EQ(obj_recv.v[2], obj_level2.v[2]);

    EXPECT_EQ(obj_recv.obj1, obj_recv.obj2->obj1);

  }

}


// ISendIRecvTest
// ------------
// Purpose: To test that any object is passed correctly from one process to
//          another. This includes the underlying objects that are pointed to
//          by the object being sent. Similar to SendRecvTest, but with
//          non-blocking send and receive operations.
//
// We set up 3 levels of a Content object. Where the top level, i.e., level 2,
// points to level 1 and level 0, and the level 1 Content object also points to
// level 0.
//

TEST(CommunicatorTests, IsendIrecvTest)
{

  // Set up object to send and receive
  std::shared_ptr<Content> obj_level0
      = std::make_shared<Content>(1.0, 2.0, 3.0, 4.0);

  std::shared_ptr<Content> obj_level1
      = std::make_shared<Content>(1.1, 2.1, 3.1, 4.1, obj_level0, nullptr);

  Content obj_level2 = Content(1.2, 2.2, 3.2, 4.2, obj_level0, obj_level1);

  // Initialize MPI
  int ierr;

  // Instatiate communicator
  Communicator comm;

  int world_size = comm.GetSize();

  // Get the rank of the process
  int world_rank = comm.GetRank();


  int tag = 12;

  if (world_rank==0)
  {

    // Send operation
    SendRequest send_request;
    send_request.BuildMessage(obj_level2);
    comm.Isend(1, tag, send_request);
  }

  // Receive operation
  if (world_rank==1)
  {
    // Probe until request is ready
    RecvRequest recv_request;
    int flag = 0;

    while (flag==false)
      flag = comm.Iprobe(0, tag, recv_request);

    std::cout << "After Iprobe, " << flag << "  " << recv_request.Size() << std::endl;
    comm.Irecv(0, tag, recv_request);

    //recv_request.WaitToFinish();
    Content obj_recv = recv_request.GetObject<Content>();

    // Test contents
    // -------------
    // Level 0
    EXPECT_EQ(obj_recv.obj1->x, obj_level0->x);

    EXPECT_EQ(obj_recv.obj1->v[0], obj_level0->v[0]);
    EXPECT_EQ(obj_recv.obj1->v[1], obj_level0->v[1]);
    EXPECT_EQ(obj_recv.obj1->v[2], obj_level0->v[2]);

    EXPECT_EQ(obj_recv.obj2->obj1->x, obj_level0->x);

    EXPECT_EQ(obj_recv.obj2->obj1->v[0], obj_level0->v[0]);
    EXPECT_EQ(obj_recv.obj2->obj1->v[1], obj_level0->v[1]);
    EXPECT_EQ(obj_recv.obj2->obj1->v[2], obj_level0->v[2]);

    // Level 1
    EXPECT_EQ(obj_recv.obj2->x, obj_level1->x);

    EXPECT_EQ(obj_recv.obj2->v[0], obj_level1->v[0]);
    EXPECT_EQ(obj_recv.obj2->v[1], obj_level1->v[1]);
    EXPECT_EQ(obj_recv.obj2->v[2], obj_level1->v[2]);

    // Level 2, i.e., top level
    EXPECT_EQ(obj_recv.x, obj_level2.x);

    EXPECT_EQ(obj_recv.v[0], obj_level2.v[0]);
    EXPECT_EQ(obj_recv.v[1], obj_level2.v[1]);
    EXPECT_EQ(obj_recv.v[2], obj_level2.v[2]);

    EXPECT_EQ(obj_recv.obj1, obj_recv.obj2->obj1);

  }

}
